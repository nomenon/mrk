﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawLine : MonoBehaviour
{
    private LineRenderer userline;
    private Vector3 initialposition = Vector3.zero;
    private Vector3 currentposition = Vector3.zero;

    List<Vector3> dir = new List<Vector3>();

    public bool inzone = false;
    bool pause = true;

    int index = 0;

    [SerializeField]
    Generator generator;

    [SerializeField]
    Transform smoke;

    float accuracy = 100.00f;

    [SerializeField]
    GameManager manager;

    void Start()
    {
        manager.LevelLoaded += ClearAll;
    }

    public void Run()
    {
        pause = true;
        initialposition = Vector3.zero;
        manager.Restart();
        StartCoroutine(DrawDelay());
    }

    IEnumerator DrawDelay()
    {
        yield return new WaitForSeconds(0.5f);
        ClearAll();
    }

    void ClearAll()
    {
        dir.Clear();
        accuracy = 100;
        index = 0;
        ActiveLine(index);
        pause = false;
    }


    public void ClearLevel()
    {
        LineRenderer[] userlines = GetComponentsInChildren<LineRenderer>();
        for (int i = 0; i < userlines.Length; i++)
        {
            userlines[i].SetVertexCount(0);
        }

        ClearAll();
        manager.ClearLevel();
    }


    void CheckCondition()
    {
        for (int i = 0; i < dir.Count; i++)
        {
            accuracy -= (dir[i] - generator.verts[i + 1]).magnitude;
        }

        manager.t_accuracy.text = accuracy.ToString("0.00") + "%";

        if (accuracy > 95)
        {
            pause = true;
            manager.StartCoroutine("Winnner");
        }

    }

    void ActiveLine(int index)
    {
        if (index < transform.childCount)
        {
            userline = transform.GetChild(index).GetComponent<LineRenderer>();
        }
        else
        {
            pause = true;
            CheckCondition();
        }
    }

    public void Update()
    {
        if (pause) return;

        if (userline != null && inzone)
        {
            if (Input.GetMouseButtonDown(0))
            {
                initialposition = GetCurrentMousePosition().GetValueOrDefault();
                userline.SetVertexCount(1);
                userline.SetPosition(0, initialposition);
            }
            else if (Input.GetMouseButton(0))
            {
                currentposition = GetCurrentMousePosition().GetValueOrDefault();
                smoke.gameObject.SetActive(true);
                smoke.position = currentposition;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                Vector3 releasePosition = GetCurrentMousePosition().GetValueOrDefault();
                Vector3 direction = releasePosition - initialposition;

                userline.SetVertexCount(2);
                userline.SetPosition(1, releasePosition);
                smoke.gameObject.SetActive(false);

                if (direction.magnitude > 0.5f)
                {
                    dir.Add(direction);
                    index++;
                    ActiveLine(index);
                    manager.ReduceParts();
                }
            }
        }
    }

    private Vector3? GetCurrentMousePosition()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var plane = new Plane(Vector3.forward, Vector3.zero);

        float rayDistance;
        if (plane.Raycast(ray, out rayDistance))
        {
            return ray.GetPoint(rayDistance);
        }

        return null;
    }
}
