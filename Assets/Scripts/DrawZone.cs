﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DrawZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    DrawLine draw;

    public void OnPointerEnter(PointerEventData eventData)
    {
        draw.inzone = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        draw.inzone = false;
    }

}
