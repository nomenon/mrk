﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    Text t_timer;
    float timer = 30;

    public Text t_accuracy;

    [SerializeField]
    GameObject gameover_panel;

    [SerializeField]
    Generator generator;

    [SerializeField]
    Text parts;
    int partscount = 0;

    [SerializeField]
    Text level;

    bool gameover = false;

    public delegate void NewLEvel();
    public event NewLEvel LevelLoaded;

    void Update()
    {
        if (gameover) return;

        timer -= Time.deltaTime;
        t_timer.text = timer.ToString("0.00");

        if (timer < 0)
        {
            t_timer.text = "GAME OVER";
            gameover_panel.SetActive(true);
            gameover = true;
        }
    }

    public void CallEventLevelLoaded()
    {
        if (LevelLoaded != null)
        {
            LevelLoaded();
        }

    }

    public void ReduceParts()
    {
        partscount--;
        parts.text = partscount.ToString();
    }


    public void Restart()
    {
        level.text = "1";
        timer = 30;
        gameover = false;
        gameover_panel.SetActive(false);
        StartCoroutine(DelayNewGame());
    }


    public void ClearLevel()
    {
        t_accuracy.text = "";
        partscount = generator.verts.Count - 1;
        parts.text = partscount.ToString();
    }

    IEnumerator DelayNewGame()
    {
        yield return new WaitForSeconds(0.5f);
        gameover = false;
        ClearLevel();
        CallEventLevelLoaded();
    }

    public IEnumerator Winnner()
    {
        gameover = true;
        yield return new WaitForSeconds(3);
        level.text = (int.Parse(level.text) + 1).ToString();
        timer = 31 - int.Parse(level.text);
        generator.Generate();
        StartCoroutine(DelayNewGame());
    }

}
