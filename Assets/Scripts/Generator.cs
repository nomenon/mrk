﻿using UnityEngine;
using System.Collections.Generic;

public class Generator : MonoBehaviour
{
    int maxhight = 6;
    int maxwidth = 6;

    [SerializeField]
    Material mat;

    Vector3 lastpos;
    Vector3 firstpos = Vector3.zero;

    LineRenderer level;

    [SerializeField]
    GameObject oneline;

    [SerializeField]
    Transform figure;

    [SerializeField]
    Transform player;

    [HideInInspector]
    public List<Vector3> verts = new List<Vector3>();

    public void Generate()
    {
        int minus;
        int countvert = Random.Range(3, 8);

        DeleteFigure();
        verts.Clear();
        verts.Add(Vector3.zero);

        for (int i = 0; i < countvert; i++)
        {
            GameObject line = Instantiate(oneline, Vector3.zero, Quaternion.identity) as GameObject;
            GameObject userline = Instantiate(oneline, Vector3.zero, Quaternion.identity) as GameObject;

            userline.transform.SetParent(player);
            line.transform.SetParent(figure);
            level = line.GetComponent<LineRenderer>();

            if (i % 2 == 0)
            {
                level.material = mat;
            }

            Vector3 position = lastpos;

            if (countvert - i == 2)
            {
                maxwidth = 4;
                minus = -1;
            }
            else
            {
                maxwidth = 6;
                minus = 1;
            }

            level.SetPosition(0, position);

            if (i != countvert - 1)
            {
                position = new Vector3(Random.Range((int)lastpos.x + 1, maxhight), minus * Random.Range(1, maxwidth), 0);
            }
            else
            {
                position = firstpos;
            }

            verts.Add(position - lastpos);
            level.SetPosition(1, position);
            lastpos = position;
        }
    }

    void DeleteFigure()
    {
        GameObject[] lines = GameObject.FindGameObjectsWithTag("Line");
        foreach (GameObject item in lines)
        {
            Destroy(item);
        }
    }

}
